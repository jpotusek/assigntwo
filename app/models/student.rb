class Student < ActiveRecord::Base
    belongs_to :school
    validates_presence_of :name
    validates :weight, :height, :gpa, :numericality => { :greater_than => 0,  :message=>"must be a positive number" } 
    validates :gpa, :numericality => { :less_than_or_equal_to => 4.33, :message=>"must be less than or equal to 4.33" }
    validates_each :name  do |record, attr, value|
    record.errors.add(attr, 'must start with upper case') if value =~ /\A[[:lower:]]/
end
  
end