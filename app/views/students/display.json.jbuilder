json.array!(@students) do |student|
  json.extract! student, :id, :name, :weight, :height, :colour, :gpa
  json.url student_url(student, format: :json)
end
